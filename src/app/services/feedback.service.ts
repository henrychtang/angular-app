import {Injectable} from '@angular/core';
import {Feedback} from '../shared/feedback';
import {Observable} from 'rxjs/Observable';
import {RestangularModule, Restangular} from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  constructor(private restangular: Restangular) {}

  getLeaders(): Observable<Feedback[]> {
    return this.restangular.all('feedbacks').getList();

  }

  getLeader(id: number): Observable<Feedback> {
    return this.restangular.one('feedbacks', id).get();
  }

  submitFeedback(feedback): Observable<Feedback> {
    return this.restangular.all('feedback').post(feedback);
  }
}
