import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Params, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

import {Dish} from '../shared/dish'
import { DishService} from '../services/dish.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishIds: number[];
  prev: number;
  next: number;
  commentForm: FormGroup;
  comment: Comment;
  formErrors = {
    'rating': '',
    'comment': '',
    'author': '',
    'date': ''
  };

  validationMessages = {
    'comment': {
      'required': 'comment is required.',
    },
    'author': {
      'required': 'Author is required.',
      'minlength': 'Author must be at leat 2 characters long.',
    },
  }

  constructor(private dishservice: DishService,
      private route: ActivatedRoute,
      private location: Location,
      private fb: FormBuilder) { 
        this.createForm();
        
      }

  ngOnInit() {
      this.dishservice.getDishIds()
      .subscribe(dishIds => this.dishIds = dishIds);
      this.route.params
        .switchMap((params: Params) => this.dishservice.getDish(+params['id']))     
        .subscribe(dish => {this.dish = dish; this.setPrevNext(dish.id); });
  }

  createForm() {
    this.commentForm = this.fb.group({
      rating: [5],
      comment: ['', [Validators.required]],
      author: ['',[ Validators.required, Validators.minLength(2)]],
      date: [''],
    });

    this.commentForm.valueChanges
        .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
  }

  onValueChanged(data?: any){
    if(!this.commentForm){return;}
    const form = this.commentForm;
    for(const field in this.formErrors){
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control && control.dirty && !control.valid){
        const messages = this.validationMessages[field];
        for(const key in control.errors){
          this.formErrors[field] +=messages[key] + '';
        }
      }
    }
  }
  
    onSubmit(){
      this.comment = this.commentForm.value;
      this.commentForm.value.date = this.getTodayDate(); 
      console.log(this.comment);
      this.dish.comments.push(this.commentForm.value);
      this.commentForm.reset({
        rating : 5,
        comment: '',
        author: '',
        date: ''
      });
    }

  setPrevNext(dishId: number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index -1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index +1)%this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  getTodayDate(): string {
    var d = new Date();
    return d.toISOString();

  }

}
